import React, { Component } from 'react';
import { Link } from 'react-router-dom'
 class SideNav extends Component {
 	handleClick=e=>{
 		e.preventDefault();
 		this.props.logout()
 	}
	render() {
		return (
			<div>
			<div id="mySidenav" style={{width:`${this.props.width}`}} className="sidenav">
  <a  className="closebtn" style={{cursor:'pointer'}} onClick={e=>{e.preventDefault(); this.props.close()}} >&times;</a>
  <Link to='/home'>Home</Link>
  <Link to='/orders'>Orders</Link>
  <Link to='/profile'>Profile</Link>
  <Link to="/notifications">Notifications</Link>
  <a href="" onClick={this.handleClick}>Logout</a>
</div>
</div>
		);
	}
}
export default SideNav