import React, { Component } from 'react';

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { error,clearFlash} from '../services/toastr/actions';
const mapStateToProps = state=>({
	
	toastr:state.toastr
})
const mapDispatchToProps = dispatch =>bindActionCreators({ 
	error,clearFlash
},dispatch)
 class Toastr extends Component {
 	handleClick=e=>{
 		e.preventDefault()
 		this.props.clearFlash()
 	}
	render() {
		if(this.props.toastr.opacity === '100'){
			setTimeout(()=>{
				this.props.clearFlash()
			},5000)
		}
		const pointer=()=>{
			if(this.props.toastr.opacity === '100')
				return 'pointer'
			return ''
		}
		const color=(s)=>{
			if(s) return 'green';
			return 'red'
		}
		return (
			<div onClick={this.handleClick} style={{cursor:`${pointer()}` ,opacity:`${this.props.toastr.opacity}`,backgroundColor:`${color(this.props.toastr.success)}`}} className='toastr'>
				<strong>{this.props.toastr.success? 'SUCCESS!!! ': 'Error!! ' }</strong><span>{this.props.toastr.message}</span>
			</div>
		);

	}
}
export default connect(mapStateToProps,mapDispatchToProps)(Toastr)
