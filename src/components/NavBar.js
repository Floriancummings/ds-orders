import React, { Component } from 'react';
import './style.css'
 class NavBar extends Component {

	render() {

		return (
			<nav className="navbar navbar-default navbar-fixed-top">
  <div className="container-fluid">
   
    <div className="navbar-header">
      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span className="sr-only">Toggle navigation</span>
        <span className="icon-bar"></span>
        <span className="icon-bar"></span>
        <span className="icon-bar"></span>
      </button>
      <div className='icon'>
      {this.props.width === '0' ? <span style={{fontSize:'30px',cursor:'pointer'}} onClick={this.props.open} >&#9776;</span>:''}
      </div>
    </div>

    
    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

      {this.props.children}
      <ul className="nav navbar-nav navbar-right">
        <li><a href="">Hello {this.props.user.username}</a></li>
        <li className="dropdown">
          <a href="" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Wallet {this.props.user.wallet} <span className="caret"></span></a>
          <ul className="dropdown-menu">
            <li><a href="" onClick={this.props.makeRequest}>Request money</a></li>
            
           
          </ul>
        </li>
      </ul>
    </div>

  </div>
</nav>
		);
	}
}
export default NavBar;