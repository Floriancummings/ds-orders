import React, { Component } from 'react';
import Landing from './scenes/Landing';
import Main from './scenes/Main';
import Register from './scenes/Register';
import Home from './scenes/Home';
import unAuth from './services/unAuth';
import requireAuth from './services/requireAuth';

import {
  Switch,
  Route,
} from 'react-router-dom';
class App extends Component {
  render() {
    return (
      <div >
       <Switch>
          <Route  exact path='/' component={unAuth(Landing)} />
          <Route  path='/orders' component={requireAuth(Main)} />
          <Route  path='/signup' component={unAuth(Register)} />
          <Route component={requireAuth(Home)} />
        </Switch>
      </div>
    );
  }
}

export default App;
