import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
//import { composeWithDevTools } from 'redux-devtools-extension';
import reducers from './services/rootReducer';
import saga from './rootSaga';
import {loadState} from './services/localStorage'
import logger from 'redux-logger';
const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducers,
	applyMiddleware(sagaMiddleware,logger)
)
sagaMiddleware.run(saga);

if(loadState()){
	store.dispatch({type:'SET_USER',user:loadState(0)})
	store.dispatch({type:'VALIDATE',token:loadState(1)})

}

export default store
