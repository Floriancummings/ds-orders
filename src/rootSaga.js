import { watchLogin,watchLogout,watchValidate,watchSignup } from './services/auth/saga';
import { watchFetchFood,watchAdd,watchRemove,watchOrder,watchMakeRequest,watchCredit } from './services/orders/saga';
import { all } from 'redux-saga/effects'

function* rootSaga() {
	yield all([
		watchLogin(),
		watchLogout(),
		watchFetchFood(),
		watchValidate(),
		watchSignup(),
		watchAdd(),
		watchRemove(),
		watchOrder(),
		watchMakeRequest(),
		watchCredit(),
		])
}
export default rootSaga;