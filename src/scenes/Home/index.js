import React, { Component } from 'react';
import NavBar from '../../components/NavBar';
import SideNav from '../../components/SideNav';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { openDrawer,closeDrawer} from '../../services/drawer/actions';
import { addFood, removeFood,clearAll,fetchFood,order} from '../../services/orders/actions';

import { error} from '../../services/toastr/actions';
import './style.css';

const mapStateToProps = state=>({
	width:state.drawer.width,	
	
})
const mapDispatchToProps = dispatch =>bindActionCreators({ 
	openDrawer,closeDrawer,addFood,removeFood,clearAll,error,fetchFood,order
},dispatch)
export class Home extends Component {
	componentDidMount(){
		this.props.fetchFood();
	}
	render() {
		
		return (
			<div className='cs'>
			<NavBar width={this.props.width} user={this.props.user}  open={this.props.openDrawer}/>
			<SideNav logout={this.props.logout} close={this.props.closeDrawer} width={this.props.width}/>
				<h1 style={{color: 'white'}}><i className="fa fa-ravelry" aria-hidden="true"></i>coming soon</h1>
			</div>

		);
	}
}
export default connect(mapStateToProps,mapDispatchToProps)(Home)