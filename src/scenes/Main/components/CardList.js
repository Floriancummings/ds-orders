import React, { Component } from 'react';
import Card from './Card'
class CardList extends Component {
	render() {
		
		return (
			<div className='list row'>
            		{this.props.orders.map((o,i)=><Card key={i} remove={this.props.remove} item ={o}/>)}

            	</div>
		);
	}
}

export default CardList;