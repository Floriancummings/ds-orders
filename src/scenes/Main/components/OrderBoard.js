import React, { Component } from 'react';
import OrderForm from './OrderForm';
import CardList from './CardList'
 class OrderBoard extends Component {
	render() {
		return (
			<div className='orders col-md-8 col-md-offset-2'>
				<OrderForm err={this.props.error} amount={this.props.amount} add={this.props.addFood}/>
         
				<CardList remove={this.props.removeFood} orders={this.props.orders}/>
				<br/><br/>
				<p> <button onClick={this.props.order} type="button" className="btn btn-success">Order</button>{'    '}
				 <button type="button" onClick={this.props.clear} className="btn btn-danger">Clear</button></p>
                

			</div>
		);
	}
}
export default OrderBoard