import React, { Component } from 'react';

 class OrderForm extends Component {
  componentDidMount(){
    this.name.focus()
  }
  handleSubmit=e=>{
      e.preventDefault();
      let price;
      if(/^\d+$/.test(this.price.value)){
        price = Number(this.price.value)
        this.props.add({name:this.name.value,price});
      }
      else 
        this.props.err('bad input')
      this.name.value =this.price.value=''
      this.name.focus();
    }
	render() {

		return (
			<div className='orderform '>
				<form onSubmit={this.handleSubmit}className="form-inline">
  <div className="form-group">
    <label htmlFor="exampleInputName2">Food</label>
    <input type="text" ref={name=>this.name= name} className="form-control" id="exampleInputName2" placeholder="Rice" required/>
  </div>
  <div className="form-group">
    <label htmlFor="exampleInputEmail2">Price</label>
    <input type="text" ref={price=>this.price= price}  className="form-control" id="exampleInputEmail2" placeholder="100" required/>
  </div>
  <button type="submit" className="btn btn-primary">Add</button>
</form>

			</div>
		);
	}
}
export default OrderForm