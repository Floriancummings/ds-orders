import React, { Component } from 'react';

class Card extends Component {
	render() {
		return (
			<div className='card col-md-3'>
			<button type="button" className="close card-dismiss" onClick={()=>this.props.remove(this.props.item.id)} aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 className='card-heading'>{this.props.item.name.toUpperCase()}</h4>
            <hr/>
            <h6 className='card-footer'>{this.props.item.price}</h6>
            </div>
		);
	}
}
export default Card