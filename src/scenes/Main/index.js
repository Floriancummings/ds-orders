import React, { Component } from 'react';
import NavBar from '../../components/NavBar';
import OrderBoard from './components/OrderBoard';
import SideNav from '../../components/SideNav';
import CreditForm from './scenes/creditUsers';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { openDrawer,closeDrawer} from '../../services/drawer/actions';
import { addFood, removeFood,clearAll,fetchFood,order,makeRequest,credit} from '../../services/orders/actions';
import Toastr from '../../components/Toastr';
import { error} from '../../services/toastr/actions';
import './style.css';

const mapStateToProps = state=>({
	width:state.drawer.width,
	orders:state.orders.orders,
	
	
})
const mapDispatchToProps = dispatch =>bindActionCreators({ 
	openDrawer,closeDrawer,addFood,removeFood,clearAll,error,fetchFood,order,makeRequest,credit
},dispatch)
export class Main extends Component {
	componentDidMount(){
		this.props.fetchFood();
	}
	makeRequest = e =>{
		e.preventDefault();
		this.props.makeRequest();

	}
	Credit=(e,data)=>{
		e.preventDefault();
		this.props.credit(data);
	}
	render() {
		
		return (
			<div className='dashboard'>
			<NavBar width={this.props.width} user={this.props.user} makeRequest={this.makeRequest} open={this.props.openDrawer}>
			{this.props.user.role === 'admin' ? <CreditForm submit={this.Credit}/> : ''}
			<SideNav logout={this.props.logout} close={this.props.closeDrawer} width={this.props.width}/>
			</NavBar>
			
			<Toastr/>
			<OrderBoard  order={this.props.order} error={this.props.error} amount={this.props.user.wallet} clear={this.props.clearAll} 
			addFood={this.props.addFood} removeFood={this.props.removeFood} orders={this.props.orders}/>
			</div>

			);
	}
}
export default connect(mapStateToProps,mapDispatchToProps)(Main)