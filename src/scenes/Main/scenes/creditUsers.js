import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';

const mapDispatchToProps = dispatch =>bindActionCreators({ 
},dispatch)
const mstp = state=>({isLoading:state.auth.isLoading})
class CreditForm extends Component {
	handleSubmit=(e)=>{

		this.props.submit(e,{amount:this.amount.value});
		this.amount.value = '';
		

	}
	render() {
		return (
			<form onSubmit={this.handleSubmit} className="navbar-form navbar-right">
        <div className="form-group">
          <input type="text" ref={amount=>this.amount= amount} className="form-control" placeholder="Amount" required/>
        </div>
        <button type="submit" className="btn btn-default">Credit</button>
      </form>
			);
	}
}
export default connect(mstp,mapDispatchToProps)(CreditForm)