import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { login} from '../../../../services/auth/actions';

const mapDispatchToProps = dispatch =>bindActionCreators({ 
login
},dispatch)
const mstp = state=>({isLoading:state.auth.isLoading})
class Login extends Component {
	handleSubmit=e=>{
		e.preventDefault();
		const data={
			username:this.username.value,
			password: this.password.value
		}
		this.props.login(data)

	}
	render() {
		return (
			<div className='signin col-md-10 col-md-offset-1'>

			<form onSubmit={this.handleSubmit}>
			
			<div className="form-group">
			<label htmlFor="exampleInputEmail1">Username</label>
			<input type="text"  ref={username=>this.username= username} className="form-control" id="exampleInputEmail1" placeholder="Username" required/>
			</div>
			<div className="form-group">
			<label htmlFor="exampleInputPassword1">Password</label>
			<input type="password" ref={password=>this.password= password}  className="form-control" id="exampleInputPassword1" placeholder="Password" required/>
			</div>


			<button type="submit" className="btn abtn form-control btn-primary" disabled={this.props.isLoading}>
			{this.props.isLoading && <i className="fa fa-spinner fa-spin" ></i>} Sign In
			</button>
			<small > Don't have an account? <Link to='/signup'>Sign Up</Link></small>
			</form>

			</div>
			);
	}
}
export default connect(mstp,mapDispatchToProps)(Login)