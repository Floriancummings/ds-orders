import React, { Component } from 'react';
import Login from '../scenes/Login';

class SideBar extends Component {
	render() {
		return (
			<div className='col-md-4 sidebar'>
			<div className="company-logo">
			<img src="http://api.packagetrack.co/files/upload/epod_logo.jpg" alt='img' className='ds'/>
		</div>
			<Login/>
			</div>
		);
	}
}

export default SideBar;