import React, { Component } from 'react';
import Sidebar from './components/SideBar';
import Pic from './components/Pic'
import './style.css';
import Toastr from '../../components/Toastr'

class Landing extends Component {
	render() {
		return (
			<div className='row'>
				<Sidebar/>
				<Pic/>
				<Toastr/>
			</div>
		);
	}
}

export default Landing