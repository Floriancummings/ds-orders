import React, { Component } from 'react';
import './style.css'
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { signup} from '../../services/auth/actions';
import Toastr from '../../components/Toastr'
const mdtp=dispatch=>bindActionCreators({signup},dispatch)
const mstp=s=>({isLoading:s.auth.isLoading})
class SignUp extends Component {
	handleSubmit=e=>{
		e.preventDefault();
		const data={
			username:this.username.value,
			email:this.email.value,
			password:this.password.value,
			number:this.number.value
		}
		this.props.signup(data)
	}
	render() {
		return (
			<div className='bg'>
			<Toastr/>
			<div className='col-md-4 signup'>
			<h2>Register</h2>
			<form onSubmit={this.handleSubmit}>
			<div className="form-group">
			<label htmlFor="username">Username</label>
			<input type="text"   ref={username=>this.username= username} className="form-control" id="username" placeholder="Username" required/>
			</div>
			<div className="form-group">
			<label htmlFor="exampleInputEmail1">Email address</label>
			<input type="email"  ref={email=>this.email= email} className="form-control" id="exampleInputEmail1" placeholder="Email" required/>
			</div>
			<div className="form-group">
			<label htmlFor="number">Phone number</label>
			<input type="text" ref={number=>this.number= number}  className="form-control" id="number" placeholder="Phone number" required/>
			</div>
			<div className="form-group">
			<label htmlFor="exampleInputPassword1">Password</label>
			<input type="password" ref={password=>this.password= password}  className="form-control" id="exampleInputPassword1" placeholder="Password" required/>
			</div>
			
			
			<button type="submit" className="btn abtn form-control btn-primary" disabled={this.props.isLoading}>
			{this.props.isLoading && <i className="fa fa-spinner fa-spin" ></i>}Sign In</button>
			<p > Have an account? <Link to='/'>Sign In</Link></p>
			</form>

			</div>
			</div>
			);
	}
}
export default connect(mstp,mdtp)(SignUp)