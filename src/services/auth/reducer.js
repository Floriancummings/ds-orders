import * as types from './actionTypes'
import isEmpty from 'lodash/isEmpty'

const init={
	isAuthenticated: false,
	user:{},
	isLoading:false
}

export default function (state=init,action) {
	switch(action.type){
		case types.SET_USER:
			
			return {
				...state, isLoading:false,
				isAuthenticated: !isEmpty(action.user),
				user:action.user
			}
		case types.LOGIN:
			return {
				...state, isLoading:true
			}
		case types.SIGNUP:
			return {
				...state, isLoading:true
			}
		case 'ERROR':
			return{
				...state,isLoading:false
			}
		default:
			return state
	}
}