import * as types from './actionTypes'

export const setUser=user=>({type:types.SET_USER,user})
export const login=data=>({type:types.LOGIN,data})
export const logout=()=>({type:types.LOGOUT})
export const signup=data=>({type:types.SIGNUP,data})