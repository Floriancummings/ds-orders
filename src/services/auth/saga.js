import { call, put, takeEvery } from 'redux-saga/effects'
import api from '../api';
import * as types from './actionTypes';
import {setState,clearState,updateState} from '../localStorage'

function* login(action){
	
	try{
		const res = yield call(api,{
			url:'/login',
			method:'put',
			data:action.data
		})
		setState(res.data)
		yield put({type:types.SET_USER,user:res.data.user})
		yield put({type:'SUCCESS',s:'You are logged in. Welcome'})

	}catch(e){
		const err = e.response ? e.response.data : "error"
		yield put({type:'ERROR',e:err})
	}
}
function* logout(){
	 clearState();
	yield put({type:types.SET_USER,user:{}})
	yield put({type:'CLOSE_DRAWER'})
}
function* validate(action){
	const token = action.token;
	try{
		const res = yield call(api,{
			url:'/validate',
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
	updateState(res.data)
	yield put({type:types.SET_USER,user:res.data})
	}catch(e){
		yield put({type:types.LOGOUT})
		console.log(e)
		const err = e.response ? e.response.data : "error"
		yield put({type:'ERROR',e:err})
	}
}
function* signup(action){
	try{
		const res = yield call(api,{
			url:'/user',
			method:'post',
			data:action.data
		})
		setState(res.data)
		yield put({type:types.SET_USER,user:res.data.user})
		yield put({type:'SUCCESS',s:'Registration successful'})

	}catch(e){
		yield put({type:'ERROR',e:e.response.data})
	}
}
export function* watchSignup(){
	yield takeEvery(types.SIGNUP,signup)
}
export function* watchValidate(){
	yield takeEvery(types.VALIDATE,validate)
}
export function* watchLogin(){
	yield takeEvery(types.LOGIN,login)
}
export function* watchLogout(){
	yield takeEvery(types.LOGOUT,logout)
}