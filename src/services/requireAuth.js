import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import {logout} from './auth/actions'
export default function(ComposedComponent) {
  class Authenticate extends React.Component {
    componentWillMount() {
      const { isAuthenticated, history } = this.props;
      if (!isAuthenticated) 
        history.push('/');
      
    }

    componentWillUpdate(nextProps) {
      if (!nextProps.isAuthenticated) {
        this.props.history.push('/');
      }
    }

    render() {
      return (
        <ComposedComponent {...this.props} />
      );
    }
  }

  Authenticate.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
  }
  function mapStateToProps(state) {
    return {
      isAuthenticated: state.auth.isAuthenticated,
      user:state.auth.user
    };
  }
  const mdtp=dispatch=>bindActionCreators({logout},dispatch)
  return withRouter(connect(mapStateToProps,mdtp)(Authenticate));
}
