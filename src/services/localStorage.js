export const loadState=(b)=>{
	const s = JSON.parse(localStorage.getItem('state'))
	if (b===0) return s.user;
	else if(b===1) return s.token;
	else return s

}

export const setState = (state) =>{
	const s = JSON.stringify(state)
	 localStorage.setItem('state',s)
}
export const clearState = ()=>{
	localStorage.removeItem('state')
}
export const updateState =user=>{
	const s = JSON.stringify({...JSON.parse(localStorage.getItem('state')),user}) ;

	localStorage.setItem('state',s)
}