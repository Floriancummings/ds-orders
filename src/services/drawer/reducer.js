import * as types from './actionTypes'
const initialState = {
	width:'0'
}
export default function(state = initialState, action){
	switch(action.type){
		case types.OPEN_DRAWER:
			return{
				width:'250px'
			}
		case types.CLOSE_DRAWER:
			return{
				width:'0'
			}
		default:
			return state
	}
}