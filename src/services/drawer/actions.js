import * as types from './actionTypes';

export const openDrawer=()=>({type:types.OPEN_DRAWER})
export const closeDrawer=()=>({type:types.CLOSE_DRAWER})