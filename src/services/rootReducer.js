import {combineReducers} from 'redux';

import orders from './orders/reducer'
import drawer from './drawer/reducer';
import toastr from './toastr/reducer';
import auth from './auth/reducer';

const reducers = combineReducers({
	drawer,
	orders,
	toastr,
	auth
})


export default reducers;
