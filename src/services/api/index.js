import axios from 'axios';
import config from './config';
import defaults from 'lodash/defaults';
const api = payload =>{
	const c = defaults(payload,{
		baseURL:config.url
	})
	return axios.request(c)
}


export default api;