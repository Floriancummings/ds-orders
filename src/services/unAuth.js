import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function(ComposedComponent) {
  class UnAuth extends React.Component {
    componentWillMount() {

      if (this.props.isAuthenticated) {
        this.props.history.push('/orders');
      }
    }

   
    componentWillUpdate(nextProps) {
      if (nextProps.isAuthenticated) 
        this.props.history.push('/orders');
      
    }

    render() {
      return (
        <ComposedComponent {...this.props} />
      );
    }
  }

  UnAuth.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
  }
  function mapStateToProps(state) {
    return {
      isAuthenticated: state.auth.isAuthenticated,
      user:state.auth.user
    };
  }

  return withRouter(connect(mapStateToProps)(UnAuth));
}
