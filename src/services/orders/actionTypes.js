export const ADD_FOOD ='ADD_FOOD';
export const REMOVE_FOOD ='REMOVE_FOOD';
export const MAKE_ORDER ='MAKE_ORDER';
export const CLEAR_ALL ='CLEAR_ALL';
export const FETCH_FOOD ='FETCH_FOOD';
export const FETCH_FOOD_COMPLETE ='FETCH_FOOD_COMPLETE';
export const MAKE_REQUEST ='MAKE_REQUEST'
export const CREDIT ='CREDIT'