import { call, put, takeEvery } from 'redux-saga/effects'
import api from '../api';
import * as types from './actionTypes';
import {loadState,updateState} from '../localStorage'


function* fetchFood(){
	try{
		const token = loadState(1)
		const res = yield call(api,{
			url:'/food',
			headers:{
				Authorization:`Bearer ${token}`
			}


		})
		console.log(res)
		yield put({type:types.FETCH_FOOD_COMPLETE,o:res.data})
	}catch(e){
		console.log(e)
	}
	
}
function* add(action){
	try{
		const token = loadState(1);
		const res = yield call(api,{
			url:'/food',
			method:'post',
			data:action.newFood,
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		console.log(res)
		updateState(res.data)
		yield put({type:'SET_USER',user:loadState(0)})
		yield put({type:types.FETCH_FOOD})
	}catch(e){
		const err = e.response ? e.response.data : "error"
		yield put({type:'ERROR',e:err})
	}
}
function* remove(action){
	try{
		const token = loadState(1);
		const res = yield call(api,{
			url:`/food/${action.id}`,
			method:'delete',
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		console.log(res)
		updateState(res.data)
		yield put({type:'SET_USER',user:loadState(0)})
		yield put({type:types.FETCH_FOOD})
	}catch(e){
		console.log(e)
	}
}
function* order(action){
	try{
		const token = loadState(1);
		const res = yield call(api,{
			url:`/makeOrder`,
			method:'put',
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		console.log(res)
		yield put({type:types.FETCH_FOOD})
	}catch(e){
		console.log(e)
	}
}
function* makeRequest(){
	try{
		const token = loadState(1);
		const res = yield call(api,{
			url:`/makeRequest`,
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		updateState(res.data)
		yield put({type:'SET_USER',user:loadState(0)})
	}catch(e){
		console.log('floerror',e)
	}
}
export function* watchOrder(){
	yield takeEvery(types.MAKE_ORDER,order)
}
function* credit(action){
	try{
		const token = loadState(1);
		const res = yield call(api,{
			url:`/credit`,
			method:'post',
			data:action.data,
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		yield put({type:'SUCCESS',s:res.data})
	}catch(e){
		yield put({type:'ERROR',e:e.response.data})
	}
}
export function* watchCredit(){
	yield takeEvery(types.CREDIT,credit)
}
export function* watchMakeRequest(){
	yield takeEvery(types.MAKE_REQUEST,makeRequest)
}

export function* watchRemove(){
	yield takeEvery(types.REMOVE_FOOD,remove)
}
export function* watchAdd(){
	yield takeEvery(types.ADD_FOOD,add)
}

export function* watchFetchFood(){
	yield takeEvery(types.FETCH_FOOD,fetchFood)
}