import * as types from './actionTypes';

export const addFood =newFood=>({type:types.ADD_FOOD,newFood});
export const removeFood =id=>({type:types.REMOVE_FOOD,id});
export const clearAll =()=>({type:types.CLEAR_ALL});
export const fetchFood=()=>({type:types.FETCH_FOOD})
export const order=()=>({type:types.MAKE_ORDER})
export const makeRequest=()=>({type:types.MAKE_REQUEST})
export const credit=(data)=>({type:types.CREDIT,data})