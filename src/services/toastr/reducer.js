
const init={
	opacity:'0',
	message:'',
	success:false
}

export default function (state=init,action) {
	switch(action.type){
		case 'ERROR':
			return{
				...state,success:false,
				opacity:'100',message:action.e
			}
		case 'CLEAR_FLASH':
			return{
				...state,
				opacity:'0'
			}
		case 'SUCCESS':
			return{
				...state,success:true,
				opacity:'100',message:action.s
			}
		default:
		 return state
	}
}