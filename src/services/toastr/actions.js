export const error=e=>({type:'ERROR',e})
export const clearFlash=()=>({type:'CLEAR_FLASH'})
export const success=s=>({type:'SUCCESS',s})